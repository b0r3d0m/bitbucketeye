import ctypes


def build_header(product_name, product_version):
    """
    Header will look like this
    ***************
    * NAME vVERS. *
    ***************
    """
    header = '{0} v{1}'.format(product_name, product_version)
    delimiter = '*'
    footer_length = len(header) + 4
    footer = delimiter * footer_length
    return footer + '\n' + '{0} {1} {0}'.format(delimiter, header) + '\n' + footer + '\n'


def get_file_content(file_path):
    with open(file_path, 'r') as f:
        return f.read()


def show_message_box(text, caption):
    MB_ICONINFORMATION = 0x40
    ctypes.windll.user32.MessageBoxA(
        0,  # A handle to the owner window of the message box to be created
            # If this parameter is NULL, the message box has no owner window
        text,
        caption,
        MB_ICONINFORMATION
    )
