![bitbucketeye logo](http://i.imgur.com/cU2BrYZ.png)

# What is it? #

**bitbucketeye** send notifications to [Slack](https://slack.com/) about any changes in repos available to you on [Bitbucket](https://highrisehq.com/)

### How to run ###

* git clone
* cd bitbucketeye
* pip install -r requirements.txt
* vim config.ini
* python main.py

### Run tests ###

* git clone, install general requirements etc
* cd tests
* pip install -r requirements.txt
* python tests.py
