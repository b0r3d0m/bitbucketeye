from requests import Session
from requests.auth import HTTPBasicAuth


# It seems that pybitbucket doesn't have an ability
# to return all repositories available for the authenticated user,
# so let's do it by ourselves by communicating with Bitbucket API directly
class Bitbucket:
    API_BASE_URL = 'https://api.bitbucket.org/1.0/'

    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.session = self.__start_session()

    def __start_session(self):
        session = Session()
        session.auth = HTTPBasicAuth(self.username, self.password)
        return session

    def get_repos(self):
        r = self.session.get(self.API_BASE_URL + 'user/repositories')
        return r.json()

    def get_events(self, owner, repo, start):
        r = self.session.get(self.API_BASE_URL +
                             'repositories/' +
                             owner +
                             '/' +
                             repo +
                             '/events?limit=50&start=' +
                             str(start))
        return r.json()['events']
